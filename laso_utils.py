# -*- coding: utf-8 -*-
import json
from os import walk, path
from typing import Dict, Any


class SignalsDatabase:
    def __init__(self) -> None:
        self.__signs_path = path.dirname(path.realpath(__file__)) + "/gifs"
        self.__list_of_signs = (
            path.dirname(path.realpath(__file__)) + "/laso_database.json"
        )
        self.__laso_signs_database = {}
        self.addtoDB_FromFolder()
        self._importDatabase()

    def _importDatabase(self) -> None:
        try:
            with open(self.__list_of_signs) as laso_json:
                self.__laso_signs_database = json.load(laso_json)
                if type(self.__laso_signs_database) == str:
                    self.__laso_signs_database = {}
                #
            #
        #
        except Exception as e:
            print(f"{e!s}: {e!r}")
            return None
        #

    #

    def getDB(self) -> Dict:
        return self.__laso_signs_database

    #

    def _updateDatabase(self) -> None:
        try:
            with open(self.__list_of_signs, "w") as laso_json:
                json.dump(
                    self.__laso_signs_database, laso_json, indent=4, ensure_ascii=False
                )
            self._importDatabase()
            #
        #
        except Exception as e:
            print(f"{e!s}: {e!r}")
            return None
        #

    #

    def addToDB(self, sign: str) -> None:
        try:
            self.__laso_signs_database[sign] = "True"
            self._updateDatabase()
        except Exception as e:
            print(f"{e!s}: {e!r}")
            return None
        #

    #

    def queryDatabase(self, sign: str) -> bool:
        if sign in self.__laso_signs_database.keys():
            return True
        #
        return False

    #

    def addtoDB_FromFolder(self) -> Any:
        path = self.__signs_path
        gifs = []
        try:
            for r, d, f in walk(path):
                for gif in f:
                    if ".gif" in gif:
                        gifs.append(gif.replace(".gif", "").strip())
                        print(gif)
                    #
                #
            #
            for gif in gifs:
                if gif not in self.__laso_signs_database.keys():
                    self.__laso_signs_database[gif] = "True"
                #
            #
        #
        except Exception as e:
            print(f"{e!s}: {e!r}")
            return None
        #
        self._updateDatabase()

    #


#
