require "file_utils"


# Definindo informações para o arquivo control
control_info ={} of String => String

control_info["Package"] = "librasoffice"
control_info["Priority"] = "optional"
control_info["Version"] = "1.0"
control_info["Architecture"] = "all"
control_info["Maintainer"] = "Laboratório de Informática e Sociedade"
control_info["depends"] = "python3, python3-pyatspi qml-module-qt-labs-platform, at-spi2-core, libatspi2.0-0, python3-pyside2.qtcore, python3-pyside2.qtqml,
python3-pyside2.qtwidgets, ython3-pyside2.qtx11extras, python3-pyside2.qtsvg, python3-pyside2.qtquick, python3-pyside2.qtquickwidgets, python3-pyside2.qtgui, python3-pyside2.qtconcurrent, qml-module-qtquick-window2, libreoffice, libreoffice-l10n-pt-br, libreoffice-help-pt-br"
control_info["Description"] = "Um leitor de telas para a Língua Brasileira de Sinais LIBRASOffice é um projeto acadêmico de extensão e pesquisa do LabIS/LIpE/UFRJ que tem como objetivo o auxílio de usuários surdos, que não dominam português, mas sim LIBRAS, na utilização de um ambiente desktop autônomamente."


# Definindo conjunto de informações para o lançador
launcher_info = {} of String => String

launcher_info["Version"] = "1.0"
launcher_info["Type"] = "Application"
launcher_info["Name"] = "LIBRASOffice"
launcher_info["Comment"] = "Um leitor de telas para a Língua Brasileira de Sinais"
launcher_info["Exec"] = "librasoffice_app"
launcher_info["Path"] = "/usr/share/LIBRASOffice" # local que o pacote deve ficar, após instalado
launcher_info["Icon"] = "/usr/share/LIBRASOffice/qml/assets/labis-librasoffice.png"
launcher_info["Categories"] = "utils"



# Definindo comando que rodará o LIBRASOffice
command_start = "python3 " + launcher_info["Path"] + "/librasoffice_app.py"


# Iniciando atividades..
puts "Criando estrutura de diretṕrios..\n"
FileUtils.mkdir_p("DEBIAN/control")
FileUtils.mkdir_p("DEBIAN/usr/share")
puts "Pronto!\n"

puts "Copiando arquivos do LIBRASOffice para os diretórios especificados.. "
FileUtils.cp_r(..)


puts command_start
