#!/usr/bin/python
import sys
from os.path import abspath, dirname, join

from PySide2.QtQml import QQmlApplicationEngine  # type: ignore
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import Slot

from copy import deepcopy
from typing import Dict, List
import time

from listener import Listener, AtSpiWatcher  # type: ignore
from accessible_place import setAccessiblePlace
from laso_utils import SignalsDatabase


class LIBRASOfficeApp(QApplication):
    def __init__(self, args: List) -> None:
        super(LIBRASOfficeApp, self).__init__(args)
        self._engine = QQmlApplicationEngine()
        self._context = self._engine.rootContext()
        self._qmlMainFile = join(dirname(__file__), "qml/main.qml")
        self._qmlPopupFile = join(dirname(__file__), "qml/popupSignal.qml")
        self._context.setContextProperty("LIBRASOfficeApp", self)

        self._time_count = time.time()

        self._signalsDB = SignalsDatabase()

        self._showingSigns = True

        self._eventCatched = dict()

        self._listener = Listener()

        self._initUi()

    #

    def _initUi(self) -> None:
        self._listener.addWatcher(AtSpiWatcher("object"), self.uiListenerConnect)
        self._engine.load(abspath(self._qmlMainFile))
        self._engine.load(abspath(self._qmlPopupFile))

    #

    def uiListenerConnect(self, event_catch: Dict) -> None:
        if self._showingSigns:
            if event_catch == False:
                self._context.setContextProperty("v", False)
                # print("Nenhum sinal para mostrar.")
            #

            elif (
                self._eventCatched != event_catch
                and event_catch["application"].strip() == "soffice"
            ):
                self._eventCatched = deepcopy(event_catch)
                if self._signalsDB.queryDatabase(self._eventCatched["content"]):
                    self._time_count = time.time()
                    self._context.setContextProperty(
                        "signToShow", self._eventCatched["content"].strip()
                    )
                    self._context.setContextProperty(
                        "legenda", self._eventCatched["content"].strip()
                    )
                    self._context.setContextProperty("v", True)
                    print(
                        "Monstrando sinal para: ", self._eventCatched["content"]
                    )  # debug
                #
                else:
                    self._time_count = time.time()
                    self._context.setContextProperty("signToShow", "nao tem sinal")
                    self._context.setContextProperty("v", True)
                    # tmp_legenda = "Sem sinal: " +
                    self._context.setContextProperty(
                        "legenda", f"Sem sinal: {self._eventCatched['content'].strip()}"
                    )
                    print(
                        "Sem sinal no banco de dados: ", self._eventCatched["content"]
                    )  # debug
            elif (time.time() - self._time_count) > 10.0:
                self._context.setContextProperty("v", False)
            #
            #

    #

    @Slot(bool, result=None)
    def handleSignalExibition(self, value: bool) -> None:
        if value == False:
            self._showingSigns = False
            print("Parei de exibir sinais")  # debug
        #
        else:
            self._showingSigns = True
            print("Exibindo sinais agora")  # debug
        #

    #

    def run(self) -> None:
        sys.exit(self.exec_())

    #


#

if __name__ == "__main__":
    setAccessiblePlace()
    app = LIBRASOfficeApp(
        ["LIBRASOffice - Um leitor de telas para a Língua Brasileira de Sinais"]
    )
    app.run()
