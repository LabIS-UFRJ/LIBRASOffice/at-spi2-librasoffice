# Sobre a "versão AT-SPI2" do LIBRASOffice

**LIBRASOffice** é um projeto acadêmico de extensão e pesquisa do LabIS 
(Laboratório de Informática e Sociedade: http://labis.cos.ufrj.br)/LIpE/UFRJ que tem como objetivo o auxílio de usuários surdos, que não dominam português, mas sim **LIBRAS**, na utilização do **LibreOffice** autônomamente.

  Originalmente o LIBRASOffice foi escrito como uma bifurcação do LibreOffice. Isso significa que uma versão do LibreOffice foi "clonada" e um software "independente"(o LIBRASOffice) foi escrito a partir dessa versão.
  O LibreOffice é um software livre com mais de 10 anos de existência, e carrega consigo cerca de centenas de milhares de linhas de código. Como já mencionado anteriormente, o LIBRASOffice é um projeto acadêmico de pesquisa e extensão, e é mantido integralmente (em termos de programação/código) por 2 alunos da UFRJ. Lidar com um software tão grande e complexo, que inclusive veio de um outro software (LibreOffice) mantido por uma imensa comunidade de desenvolvedores, não estava sendo uma tarefa viável.

  Ao decidirmos que o desenvolvimento da versão do LIBRASOffice bifurcada do LibreOffice não seria mais continuada, outras alternativas para desenvolvimento foram encontradas. Os sistemas operacionais Linux tem uma tecnologia assistiva padrão chamada AT-SPI2 (https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/), que permite o acesso à elementos textuais (ex: O nome do botão "Salvar", ou textos/palavras escritos dentro do LibreOffice, navegador web, etc) e eventos em geral da interface dos softwares (ex: dicas de ferramentas, quando um programa abre ou fecha, quando é minimizado, quando um aviso é mostrado..) instalados e o sistema operacional. A AT-API2 já era/é utilizada em leitores de tela para pessoas com deficiência visual, no linux. Um exemplo: https://help.gnome.org/users/orca/stable/index.html.pt_BR.

Esse repositório guarda a versão do LIBRASOffice feita com a AT-SPI2. Que em comparação com a versão que era bifurcada do LibreOffice, nós não temos que:

* Passar por uma compilação de 5-7 horas com frequência
* Nos preocupar em portar cada versão nova do LibreOffice para LIBRASOffice (as alterações necessárias mudam a ponto de parecer um trabalho desde o 0)
* Ser menos de 5 pessoas lidando com o desenvolvimento de um software que originalmente era/precisava ser mantido por uma comunidade imensa de desenvolvedores

Infelizmente ainda não existe uma tecnologia assistiva que seja multiplataforma :(
Isso quer dizer que a versão do LIBRASOffice nesse repositório só funciona para sistemas operacionais linux que são compatíveis com a tecnologia assistiva AT-SPI2 (se não todos, quase todos).

O Windows também tem uma tecnologia assistiva padrão, que inclusive também pode ser usada para automação de atividades no computador (ex: criar um bot que controla o firefox). O nome da tecnologia assistiva padrão do Windows é Microsoft UI Automation (https://docs.microsoft.com/en-us/windows/win32/winauto/entry-uiauto-win32).

Uma versão do LIBRASOffice para Windows está sendo criada, utilizando a UI Automation. Mas para tudo isso aqui, toda a ajuda é bem vinda :) Principalmente de código... isso aqui dá um trabalhão, mas é divertido :)


# Guia para desenvolvedores (Quick Stard Guide):

**Instale o Python 3 e alguns módulos**

Dependências que devem ser instaladas pelo **apt**:

```
sudo apt install git python3 python3-pip python3-venv python3-pyatspi qml-module-qt-labs-platform
```

É recomendado que um "ambiente virtual python" isolado seja criado para esse projeto. Para isso:
Abra o terminal e digite:

```
python3 -m venv --system-site-packages caminho/para/o/seu/novo/ambiente (não faça isso na pasta do clone desse repositório)
```

Clone esse repositório no local de sua escolha.
Crie uma pasta nesse local, e abra o terminal nela. Digite:
```
git clone https://gitlab.com/LabIS-UFRJ/LIBRASOffice/at-spi2-librasoffice.git
```
Entre na pasta clonada. No mesmo terminal/mesmo lugar, digite:

```
cd at-spi2-librasoffice
```

**Instale os requisitos do python**

Algumas dependências são pacotes python e devem ser obtidas através do pip.
Há poucos passos atrás, você havia criado o seu ambiente python isolado para esse projeto. Chegou a hora de usá-lo. Para isso, você precisa ativá-lo. Nesse mesmo terminal que você abriu e está dentro da pasta do projeto, você pode digitar:
```
source caminho/para/o/seu/novo/ambiente/bin/activate
```

Pronto. O seu ambiente python isolado está ativado e você está pronte para instalar os pacotes python. Digite isso com o terminal aberto dentro da pasta do projeto:

```
pip3 install -r requirements.txt (dentro da pasta que você acabou de clonar)
```

**Para rodar**
```
python3 librasoffice_app.py (com o seu ambiente isolado python ativado)
```

**Use:**

* **Black** - https://black.readthedocs.io/en/stable/the_black_code_style.html para formatação de código
* **Docstrings** - https://www.python.org/dev/peps/pep-0257/ para documentação
* **PyLint** -https://www.pylint.org/ para análise e verificação de qualidade do código


**Dica**

Instale o Atom (https://atom.io/) e as extensões:

1. **language-qml** - Para colorir a sintaxe do qml
2. **linter-pylint** - Para utilizar o PyLint
3. **python-black** - Para utilizar o Black



>Para mais informações (sobre desenvolvimento ou em geral), envie um e-mail para labis@cos.ufrj.br

# Documentação Avançada

# Arquitetura Geral

## O Padrão Observer

Fazendo uso de sua definição formal, o Padrão Observer "*define uma dependência um-para-muitos entre os objetos de modo que, quando um objeto muda de estado, todos os seus dependentes são notificados e atualizados automaticamente*”. Isso quer dizer que esse padrão de projeto permite a integração entre dois ou mais objetos que interajam um com o outro, mas sem criar uma forte dependência entre eles.
Mas o que isso quer dizer?
O Padrão Observer adere ao Princípio da Ligação Leve, em que se busca designs levemente ligados. Isso quer dizer que os objetos podem interagir entre si normalmente, mas sabendo muito pouco um do outro. Essa característica garante uma maior flexibilidade de projeto, já que podemos adicionar, substituir, alterar ou remover Observers no projeto sem alterar o comportamento ou a estrutura do Subject.

Dentro do LIBRASOffice, utilizamos esse padrão na integração entre o LibreOffice e a biblioteca de GIFs associados a cada comando. Quando passamos o mouse sobre um ícone, o nosso software filtra o nome (ou a legenda que aparece sob ele, caso seja uma imagem) e busca pela tradução em nossa biblioteca, exibindo a imagem correspondente no canto inferior da tela. Caso a palavra buscada não esteja contida em nosso acervo, o GIF exibido corresponde a mensagem "*Esse sinal ainda não foi gravado*".

## As APIs de Acessibilidade

Essas são as ferramentas que nos possibilitam implementar o padrão Observer a nível de sistema operacional e que consistem no resgate e captura de eventos e demais interações com os elementos de interface do computador. Isso significa, portanto, que quando o mouse passa em cima de uma *tooltip*, por exemplo, o sistema, se tiver o recurso de acessibilidade instalado, irá registrar tal ocorrência (geralmente com informações de posicionamento de mouse) programa no qual o evento aconteceu, entre demais informações que podem ser úteis para o processo de criação de uma aplicação de acessibilidade como o LIBRASOffice - *screen readers* mundo afora também fazem uso desses dados para o seu funcionamento.

Sabendo disso, falemos então brevemente sobre as APIs em si. Naturalmente, cada uma das duas versões do LIBRASOffice utiliza a sua própria interface, desenvolvida de acordo com a sua própria arquitetura. 

No Windows, temos a utilização da [UI Automation](https://docs.microsoft.com/en-us/windows/win32/winauto/entry-uiauto-win32) - que sucede uma especificação anterior, conhecida como MSAA.

Já no Linux, em especial em ambientes GNOME, a interface predominante é a [ATSPI](https://gitlab.gnome.org/GNOME/at-spi2-core/-/blob/master/README), que opera em cima de um "mecanismo de comunicação entre processos" chamado [DBus](https://www.freedesktop.org/wiki/Software/dbus/). 

Cada uma dessas duas ferramentas é abordada com mais detalhes na sua respectiva documentação, onde serão consideradas questões como instalação e configuração.

## Submódulos

Submódulos são bem intuitivos - nada mais são do que repositórios dentro de um outro repositório. Sua utilização é, sobretudo, definida pela arquitetura do projeto, podendo prover certa independência a diferentes partes de uma aplicação, o que, por sua vez, facilita a manutenção e a identificação de erros.

### Gifs

A nossa biblioteca de GIFs conta com uma coletânea de arquivos de imagem, preparados pela nossa equipe com a ajuda de um tradutor humano. Neles, traduzimos visualmente os principais comandos do LibreOffice para libras, e é nesse repositório que o nosso software realiza as buscas mencionadas ao final da seção "Padrão Observer". O projeto intenciona a total tradução do LibreOffice, mas priorizamos as funcionalidades mais utilizadas.

### Windows Listener ([link](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/eventlistener-win))

Esse módulo é responsável por implementar uma abstração de um *listener* em sistemas Windows, já pensando na formatação com que a API de acessibilidade da Microsoft emite seus eventos. O README disponível no repositório demonstra o seu uso e pode ser um bom ponto de partida para compreender o que o LIBRASOffice utiliza por trás dos panos.  

## O que é o QT? ([link](https://www.qt.io/product))

Qt é um framework multiplataforma para desenvolvimento de interfaces gráficas em C++ criado pela empresa norueguesa Trolltech. Com ele, é possível desenvolver aplicativos e bibliotecas uma única vez e compilá-los para diversas plataformas sem que seja necessário alterar o código fonte. 

Famoso no mundo open source - apesar de recentemente ter sido adquirido por uma empresa, o que acabou por tornar alguns de seus recursos privados - o Qt ganha destaque por sua versatilidade e variedade de ferramentas, possibilitando-o a ser utilizado no desenvolvimento de softwares desktop, mobile e até mesmo os "embarcados", como é o caso de aplicações para carros e outros sistemas de eletrodomésticos inteligentes.

Apesar de sua concepção em C++, seu uso provocou a criação de vários bindings - algo similar a um porte - para várias linguagens, como é o caso do Python. No LIBRASOffice, utilizamos essas duas linguagens, uma em cada versão, mas sempre compartilhando das ferramentas que o framework Qt nos proporciona.

### QML ([link](https://www.notion.so/Arquitetura-Geral-a8f87b89649d4391a6ea838935a94180))

QML é uma linguagem de marcação de texto, assim como o HTML, voltado para a criação de interfaces gráficas. Como a letra 'Q' acusa, a linguagem foi criada pensando no ambiente de desenvolvimento do Qt, a fim de trazer mais fluidez e dinâmica nos elementos gráficos das aplicações. 

Sua estrutura de código é bastante similar à do JSON, enquanto muitas das suas propriedades e lógicas de customização nos remetem imediatamente ao CSS. Veja um exemplo do próprio projeto abaixo.

![qml_example](doc_images/qml_example.png)

# ============== Versão Linux ==============

# Introdução

### Distribuição

Falar sobre Linux naturalmente envolve a sua grande quantidade de distribuições. Inicialmente, esse projeto foi concebido utilizando o sistema operacional KDE Neon, baseado no Ubuntu. Esse último - por ser a distro mais comum - foi escolhida como alvo para a disponibilização do programa, resultando na criação de um pacote DEBIAN. Dessa forma, estamos restritos a rodar o programa em distribuições que sejam, além do próprio Ubuntu, uma derivação sua.

### Python e Pyside

Nessa versão do programa, utilizamos o python como linguagem principal. Como vimos na seção de arquitetura geral, o Qt não é natural do python, então é necessário utilizarmos um binding (podemos entender, em alto nível, como um porte), e é aí que entra o módulo oficial de adpatação à linguagem, o [PySide2](https://pypi.org/project/PySide2/). No link a seguir, podemos acessar a documentação oficial do pacote [https://doc.qt.io/qtforpython/](https://doc.qt.io/qtforpython/).

Dentre todos os componentes deste módulo, os que mais utilizaremos serão QtCore (que como o nome já diz, consiste das estruturas fundamentais para o Qt, como é o caso de [signals e slots](https://doc.qt.io/qt-5/signalsandslots.html) ) e o QtWidgets, que por sua vez é o cerne de elementos gráficos de qualquer aplicação construída com essa framework.

Obs: signal e slots são basicamente componentes que visam a emissão e detecção de eventos, respectivamente. No nosso caso, essa é uma ótima ferramenta para implementação do padrão observable no projeto. 

# Dependências e Programas + Primeira Execução

Em primeiro lugar, baixamos o projeto e seus submódulos de maneira tradicional. Abra um terminal e rode os comandos:

```bash
$ git clone https://gitlab.com/LabIS-UFRJ/LIBRASOffice/at-spi2-librasoffice.git

#Com o projeto já baixado, dentro dele, rode:

$ git submodule init
$ git submodule update
```

Bom, vamos precisar fazer o download de alguns pacotes que são fundamentais para o LIBRASOffice. Nesse primeiro momento, o foco é configurar nosso ambiente de desenvolvimento, então uma abordagem mais  detalhada fica para um momento posterior.

Começamos então com o download de bibliotecas, usando o gerenciador de instalações do Python 3, o pip3. Digite no terminal:

```bash
pip3 install pyside2 #um dos bindings de QT para o Python
```

Agora partimos para as especificações a respeito da acessibilidade no sistema. Aqui cabe a introdução de um repositório feito justamente para poder monitorar a acessibilidade do sistema. Com ele, é possível verificar quais bibliotecas estão presentes e quais variáveis de ambiente precisam ser devidamente setadas. Em um local de escolha, faça o seguinte:

```bash
$ git clone https://git.debian.org/git/pkg-a11y/check-a11y
$ cd check-a11y
$ ./troubleshoot
```

Os pontos mais importantes aqui serão a verificação do pacote pyatspi-core, logo na parte inicial do log, bem como todos os apontamentos feitos na parte final, sob a alcunha de Layer Enabling.

Por padrão, algumas dessas variáveis não vêm ativadas, e de tal maneira, vamos fazer isso manualmente. 

```bash
export GTK_MODULES=gail:atk-bridge
export OOO_FORCE_DESKTOP=gnome
export GNOME_ACCESSIBILITY=1
export QT_ACCESSIBILITY=1
export QT_LINUX_ACCESSIBILITY_ALWAYS_ON=1
export ACCESSIBILITY_ENABLED=1
```

Com essas alterações, tudo que precisamos agora é baixar o LibreOffice - se ainda não o tiver - entrar na pasta do repositório do projeto, e rodar o aplicativo com o comando abaixo.

```bash
$ python3 librasoffice_app.py
```

# ATSPI2

Peça fundamental da nossa arquitetura, a API de acessibilidade fornecida pelos desenvolvedores de distribuições Linux é o ATSPI2. Sinceramente, a melhor forma de conhecer essa tecnologia é lendo sua documentação oficial. É claro que não é necessário conhecer tudo num único dia; o objetivo é conhecer e entender o motivo de sua existência e como isso se manifesta em seu funcionamento. Para essa tarefa de familiarização com o ambiente, recomendo a leitura desses dois documentos:

- Arquitetura geral de acessibilidade: [https://www.freedesktop.org/wiki/Accessibility/](https://www.freedesktop.org/wiki/Accessibility/)
- Documentação generalista a respeito do ATSPI 2: [https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/](https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/)

Depois de acostumadx com o cenário, podemos partir para uma cobertura mais prática, já estudando o porte dessa tecnologia para o Python - com a lib *pyatspi.* Em especial, é interessante saber como se comportam algumas partes da API e também quais são os eventos que conseguimos observar utilizando-a. Para isso, deixo como referência dois links: uma para o esquema de registro de watchers, algo que utilizaremos bastante no arquivo *[Listener.py](http://listener.py)* e que visitamos rapidamente na sessão de configuração.

- Registry: [https://people.gnome.org/~parente/pyatspi/doc/](https://people.gnome.org/~parente/pyatspi/doc/)
- Lista de eventos: [https://accessibility.linuxfoundation.org/a11yspecs/atspi/adoc/atspi-events.html](https://accessibility.linuxfoundation.org/a11yspecs/atspi/adoc/atspi-events.html)

# Código

## event_filter.py

Este arquivo é um dos corações do LIBRASOffice. Com um nome também intuitivo, a classe aqui descrita é responsável por filtrar os eventos registrados pelo ATSPI2. 

Aqui cabe um longo parênteses, para podermos explicar minimamente o formato das mensagens jogadas pelo aparato assistivo do Gnome. Como teste, crie um arquivo python rapidamente e insira o código a seguir:

```python

#Importamos a lib *pyatspi* que facilita a nossa interação por meio do Python para com os eventos capturados pelo ATSPI. 
import pyatspi 

#Criamos uma função, que nesse caso é genérica, para poder imprimir seu argumento
def f(e):
    print(e) ;

"""Associamos a nossa função de print ao objeto de Listeners do ATSPI2,
	 passando-a como primeiro argumento, enquanto o segundo parâmetro se trata
	 de uma classificação de eventos que queremos escutar.
	 No caso abaixo, iremos perceber eventos relacionados a "janelas", como 
	 maximização, fechamento, abertura, entre outros eventos particulares 
	 desse tipo de objeto.
"""
pyatspi.Registry.registerEventListener(f, "window");

#Iniciamos o monitoramento
pyatspi.Registry.start();
```

Se você rodar esse pequeno arquivo e realizar algumas ações em sua máquina, como minimizar o terminal, abrir um navegador, etc, poderá ver uma impressão parecida com essa:

![eventos sem filtros](doc_images/eventos_sem_filtros.png)

Ok, ok, não se assuste. Essa é apenas a maneira com que o definimos a impressão de uma **classe** de eventos. Perceba que ao lado de cada evento *window* impresso, há uma outra descrição, a exemplo de *deactivate, minimize.* Pois bem, essa segunda etapa também faz parte uma outra propriedade da classe window.

No geral, podemos capturar eventos com o seguinte formato: **class:major:minor.** A lista de todos os eventos pode ser verificada no link [https://accessibility.linuxfoundation.org/a11yspecs/atspi/adoc/atspi-events.html](https://accessibility.linuxfoundation.org/a11yspecs/atspi/adoc/atspi-events.html), e recomendo fortemente que o faça para se ambientar com as ações que somos capazes de capturar. Como você verá, são muitas, é por isso que necessitamos de um filtro.

Antes de prosseguir, cabe salientar que nem todas as informações impressas são estritamente necessárias pra gente, como veremos na cobertura do código.

Bom, para o LIBRASOffice, desejamos acompanhar duas ações primordiais do usuário: a interação com itens de menu, chamadas geralmente de *tooltips,* e a interação com as opções da barra de ferramentas do programa - mais especificamente, com os momentos em que o usuário foca seu cursor por lá. Esses eventos estão dispostos no formato esperado do ATSPI2 no próprio código, como podemos observar a seguir:

```python

#Supondo um evento com "object:...".
self._noticedEvents = (
            "state-changed:focused",
            "state-changed:selected",
            "state-changed:visible",
            "state-changed:showing",
            "property-change:accessible-name",
        )

#Exemplo de evento completo: "object:state-changed:focused"
```

### Função eventTriage

Na própria função temos uma descrição bem sintética da responsabilidade do método, mas acredito que valha a pena passar por ele já que está diretamente relacionado com as descrições que vimos acima. Outro detalhe é que essa função está diretamente ligada à *getEventCatched,* um método utilitário e intermediário.

Esse trecho abaixo é ótimo para exemplificar como pegamos uma informação como aquela mostrada na imagem e transformamos num objeto mais palpável para o nosso trabalho. Nó

```python
if actionEvent in "focused, selected":  # get menu and popup menu events
                    eventAtspiProp["name"] = event.source.get_role_name()
                    eventAtspiProp[
                        "application"
                    ] = event.source.get_application().get_name()
                    eventAtspiProp["eventType"] = event.type
                    eventAtspiProp["content"] = self.cleanseEventContent(
                        event.source.get_name()
                    )
                #
```

Exemplificando, temos abaixo como ficariam o próprio objeto *eventAtspiProp* como também o seu atributo *content* - que, por sua vez, é modelado com uma função auxiliar chamada *cleanseEventContent,* cuja utilidade está principalmente em retirar os atalhos embutidos nos eventos que os possuem. 

```python
#Content antes da função cleanse
Raw Content: Abrir (Ctrl+O)

#Content depois de cleanse
Cleanse-modified Content: abrir

#Objeto criado pela função eventTriage
eventAtspiProp: {'name': 'tool tip', 'application': 'soffice.bin', 'eventType': 'object:property-change:accessible-name', 'content': 'abrir'}
```

Um detalhe importante é perceber que os eventos do LibreOffice geralmente vêm com o nome de aplicação *'soffice.bin'.* 

Ainda sobre essa função, temos aqui a consideração (e separação em casos) dos dois maiores tipos de eventos para nós: tooltip e itens de menu/sub-menu. 

## Listener.py

Outro componente fundamental - o nosso "escutador de eventos" - está presente aqui, segmentado em duas classes. Nesse momento, há uma maior participação de componentes do Qt, os quais serão comentados conforme o necessário. Vamos visitar cada uma das classes e seus métodos e características com mais destaque. Última observação: é interessante estar familiarizadx com os eventos descritos no arquivo event_filter.py.

### class AtSpiWatcher

Começando com o que já estamos familiarizados, vamos visitar rapidamente o método *startWatching.* Sim, já vimos essa mesma estrutura antes! A ideia aqui é criar uma função personalizada e posteriormente registrá-la como um "observador" dos eventos do computador. 

```python
def startWatching(self) -> None:
        pyatspi.Registry.registerEventListener(self.eventDetectedEmiter, self.eventTree)
        pyatspi.Registry.start(asynchronous=True)
    #
```

No caso acima, a função customizada é a *eventDetectedEmiter.* Esse método está diretamente relacionado com um atributo da classe (*eventDetected*)*.* Essa propriedade é criada a partir de um Signal, um objeto central do framework Qt, e que está relacionado ao seu complementar, o Slot. Basicamente, um objeto do tipo Signal pode emitir um evento (daí a palavra "sinal"), o qual pode ser capturado por um "slot". É como acender um sinalizador sabendo que alguém certamente vai perceber. Para mais detalhes, fica o [link da documentação oficial](https://doc.qt.io/qt-5/signalsandslots.html) e mais um [recurso](https://www.riverbankcomputing.com/static/Docs/PyQt5/signals_slots.html). Nesse momento, o que é mais importante saber é de que um sinal pode emitir um evento e ativar um determinado callback. É exatamente isso que fazemos agora.

```python
def eventDetectedEmiter(self, event) -> None:
	self.eventDetected.emit(self.eventFilter.getEventCatched(event))
```

### class Listener

Assim como a classe anterior, essa também herda da classe [QObject](https://doc.qt.io/qtforpython-5/PySide2/QtCore/QObject.html), uma das mais gerais de toda a framework Qt. Esse característica merece realce nesse momento porque, para implementar o nosso "watcher", utilizaremos um conceito de *threads,* e consequentemente, de paralelismo. Para poder entender melhor o básico desse assunto, temos um ótimo texto do pessoal do Real Python sobre o assunto, focando num problema específico de congelamento de interface gráfica, mas cobrindo o panorama geral de por que threads são importantes: [https://realpython.com/python-pyqt-qthread/](https://realpython.com/python-pyqt-qthread/). O artigo utiliza uma biblioteca PyQt, que é bem similar ao Pyside, então praticamente tudo é adaptável para a nossa realidade.

Bom, o que acontece é que precisamos criar um segmento de execução à parte da thread principal do programa para rodar nosso listener. Vamos fazer isso utilizando uma classe chamada [QThread](https://doc.qt.io/qtforpython-5/PySide2/QtCore/QThread.html), com a qual objetos QObject conseguem interagir facilmente (daí a importância dessa herança na classe Listener).

```python
def __init__(self) -> None:
        super(Listener, self).__init__()
				
				# criação da thread herdando de QThread
        self._listenerThread = QThread(self) 

def addWatcher(self, watcher: AtSpiWatcher, uiConnect) -> None:	
				
				# adiciona watcher à lista de watchers
        self._watchers.append(watcher)
				
				# adiciona o watcher passado à corrente de execução auxiliar
        watcher.moveToThread(self._listenerThread)
        
				# estabelece que quando a thread for iniciada, a função
				# startWatching também o será; além de criar esse link entre elas
				self._listenerThread.started.connect(watcher.startWatching)

				# "liga" a thread
        self._listenerThread.start()
```

## acessible_place.py

Esse arquivo possui a intenção de automatizar o processo de configuração de acessibilidade no ambiente de desenvolvimento, de maneira a evitar o trabalho manual desse processo. No entanto, ele não está funcional no momento, pelo menos não para sistemas KDE. No próprio arquivo há uma breve descrição em inglês da sua funcionalidade e intenção.

## environ_props.py

Com um nome relativamente autoexplicativo, este é um arquivo orientado ao mapeamento de configurações de ambiente. Mais precisamente, criamos uma classe chamada de EnvironProps, que por sua vez utilizará o objeto *os.environ -* que coleta todas as variáveis de ambiente públicas do sistema operacional - para podermos criar uma instância somente com as informações que desejamos. Desejamos então recuperar identificações de sessão, da área de trabalho sendo efetivamente utilizada da sessão de DBus e do idioma da máquina.

Com exceção do idioma, todas as outras variáveis são relativas a configurações necessárias para uma comunicação entre a API de acessibilidade e o nosso programa. Vamos falar brevemente sobre a questão da linguagem, e depois partir para um assunto mais complexo e (mais) importante: o DBus.

```python
@classmethod
def getSystemLanguage(cls,self) -> str:
	return environ['LANG'].split(".")[0];
```

Capturamos o idioma por um motivo bem simples e direto, que é o fato do LIBRASOffice só funcionar com a versão em português do LibreOffice. Quando capturamos eventos, todos eles precisam estar em português para que possamos relacionar os sinais em Libras às ocorrências no programa. Como não é possível descobrir a língua do próprio programa, verificamos a língua principal do sistema e emitimos um aviso para que o usuário se certifique de que a sua versão do LibreOffice também está em português. 

A outra função desse arquivo que vale comentar é a *_isValidDBusSessionAddress.* O DBus é basicamente um meio de comunicação entre processos do Linux. Pela natureza do ATSPI e da nossa aplicação, nós precisamos que o segundo possa interagir com o primeiro por meio do DBus, e por isso, precisamos cuidar desse mecanismo. Acontece que o DBus é configurado através de sessões, o que significa que podemos ter mais de uma ao mesmo tempo. Nesse mesmo arquivo, há uma função para poder pegar a propriedade *DBusSessionAddress* diretamente da coleção de variáveis de ambiente. O problema é que nem sempre essa sessão é a mesma sobre a qual está rodando o ATSPI, o que exige checagem.

No seu diretório "/home/{usr}/.dbus/session-bus/", é bem provável que se encontre mais de uma sessão. A primeira costuma ser aquela que desejamos, e caso o script de *troubleshoot (do check-a11y)* denuncie um "MISMATCH", é necessário setá-la também como variável de ambiente. Bom, é isso que o código da função abaixo faz: verifica igualdade entre a sessão guardada no arquivo e a sessão do ambiente. Fica a recomendação aqui de tentar dar uma olhada nesses arquivos que a função verifica para se compreender melhor o que acontece por trás.

```python
@classmethod
    def _isValidDBusSessionAddress(cls, self) -> bool:
        if self.dbusSessionAddress == None:
            return False
        #
        displayNumber = environ["DISPLAY"].replace(":", "")
        homeAddress = environ["HOME"]
        machineIDcmd = "echo $(cat /var/lib/dbus/machine-id)"
        process = Popen(machineIDcmd, stdout=PIPE, shell=True)
        machineID, error = process.communicate()
        dbusFileAddress = (
            homeAddress
            + "/.dbus/session-bus/"
            + str(machineID).replace("\\n", "").replace("b'", "").replace("'", "")
            + "-"
            + displayNumber
        )
        try:
            with open(dbusFileAddress) as file:
                for line in file:
                    if line[0] != "#":
                        if "DBUS_SESSION_BUS_ADDRESS" in line:
                            if self.dbusSessionAddress in line:
                                return True
                            #
                            return False  # dbus session in file not correspond to bus session in env variable
                        #
                return False
            #
        #
        except FileNotFoundError as error:
            print("Could not find the file that holds DBus session data:", error)
            return False
        #

    #
```

## librasoffice_app.py

### initUi

UI é o acrônimo de User Interface, e por intuição, pode-se entender que essa função é responsável por inicializar todas as características gráficas do programa: é ela quem vai exibir os e demais elementos feitos com o QML. Não à toa, os widgets feitos são carregados a partir de uma *[QQMLApplicationEngine](https://doc.qt.io/qtforpython-5/PySide2/QtQml/QQmlApplicationEngine.html),* importada diretamente do Pyside2.

```python
from PySide2.QtQml import QQmlApplicationEngine

#Dentro do init
self._engine = QQmlApplicationEngine()

#Função initUi
def _initUi(self) -> None:
        if(self.env.systemLocale != 'pt_BR'): 
            self._engine.load(abspath(self._qmlLanguageWarning))
        
        self._engine.load(abspath(self._qmlMainFile))
        self._engine.load(abspath(self._qmlPopupFile))
```

Os widgets são importados lá em cima, junto com o módulo *[os](https://www.geeksforgeeks.org/os-module-python-examples/),* que facilita a obtenção de caminhos e sistemas de pastas em seu sistema operacional. É dele que vem essa função *abspath.*

### uiListenerConnect

Precisamos falar sobre **[context](https://doc.qt.io/qt-5/qqmlcontext.html)**. A ideia principal é que consigamos passar alguns valores dos eventos que capturamos para nossas telas de interface gráfica. Assim, é possível mostrar para um usuário qual é a função sendo descrita em Libras naquele momento, bem como o seu respectivo sinal - além da duração de tal (indicado pela barrinha vermelha). Toda essa interação é responsabilidade do método vigente, e complementa o método anterior. Enquanto lá nós preparamos os elementos gráficos, aqui é onde passamos os parâmetros necessários a eles.

```python
def __init__(self, args: List) -> None:
	super(LIBRASOfficeApp, self).__init__(args)
	
	# Setando um atributo com o contexto raiz do gerenciador gráfico
	self._context = self._engine.rootContext()
```

A função *uiListenerConnect* é grande, mas bem simples; por isso, acredito que o melhor seja lê-la um pouco mesmo. Um adianto, porém, é pontuar que aqui verificamos se os sinais estão ou não em nossa database, e também fazemos parte da lógica de duração de exibição do sinal, característica intrínseca à barra de progresso vermelha na interface do sinal. Outro lembrete é que o método *setContextProperty* geralmente lida com valores no formato de chave-valor.

### handleSignalExhibition

Essa é uma função bem simples, acoplada diretamente ao *system tray -* a parte no extremo direito da Barra de Tarefas, onde podemos desmontar pen drives, e ver algumas configurações de internet.

![system_tray](doc_images/sys_tray.png)

Quando o programa é iniciado, seu ícone ficará junto desses símbolos, dando a opção de ligar e desligar a exibição dos sinais. 

### run

É um método bem direto, mas gostaria apenas de comentar algo que dá continuidade ao nosso papo sobre threads, iniciado no arquivo Listener.py; essa é a função que executa a thread principal do sistema!
