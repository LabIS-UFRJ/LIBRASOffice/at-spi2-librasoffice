#!/usr/bin/python
"""
Makes the process of "enabling accessibility" easier in linux environments

This module "automates" the process of enabling accessibility in Linux
environments using AT-SPI2. Setting necessary variables .. solving recurring
"small problems" and etc. After the function present in this module is executed,
screen readers will have access to events and textual elements of  programs
running on DBus.

Typical usage example:

    from accessible_place import setAccessiblePlace

    setAccessiblePlace() # And voilà! Enjoy it!

"""

from environ_props import EnvironProps
from subprocess import Popen, PIPE, STDOUT
from os import environ


def setAccessiblePlace() -> None:
    env = EnvironProps()
    if env.osPlatform == "linux":
        if env.isValidDBusSessionAddress == False:
            dbusFix = Popen(
                "export $(dbus-launch)", shell=True, stdout=PIPE, stderr=STDOUT
            )
            try:
                assert dbusFix.stderr == None
            except AssertionError as e:
                print(
                    "Something wrong happened while DBus session address was being corrected.",
                    e,
                )
            #
        #
        accKDEGnome = Popen(
            "gsettings set org.gnome.desktop.a11y.applications screen-reader-enabled false",
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
        )
        accKDE = Popen(
            "kwriteconfig5 --file kaccessrc --group ScreenReader --key Enabled true",
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
        )
        environ["GTK_MODULES"] = "gail:atk-bridge"
        environ["GNOME_ACCESSIBILITY"] = "1"
        environ["QT_ACCESSIBILITY"] = "1"
        environ["QT_LINUX_ACCESSIBILITY_ALWAYS_ON"] = "1"
        environ["ACCESSIBILITY_ENABLE"] = "1"
    #


#
