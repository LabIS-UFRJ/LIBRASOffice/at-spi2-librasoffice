import QtQuick 2.12
import QtQuick.Window 2.12


Window {
    id: librasoffice
    width: Screen.desktopAvailableWidth/100 * 15
    height: Screen.desktopAvailableHeight/100 * 35
    visible: v
    flags: Qt.Popup
    y: Screen.desktopAvailableHeight - height
    x: Screen.desktopAvailableWidth - width

    Rectangle {
        id: rectangle
        width: parent.width
        height: parent.height
        color: "white"

        AnimatedImage {
            id: gif
            width: rectangle.width
            height: rectangle.height/100 * 85
            source: '../gifs/' + signToShow + '.gif'
        }

        Rectangle {
            property int frames: gif.frameCount
            width: 4; height: 8
            x: (gif.width - width) * gif.currentFrame / frames
            y: gif.height
            color: "orange"
        }

        Rectangle {
            id: rectangle_legenda
            property string legenda: ""
            width: rectangle.width
            height: rectangle.height/100 * 10
            y: gif.height + 8
            Text{
                font.capitalization: Font.Capitalize
                text: legenda
                font.family: "Helvetica"
                font.pointSize: rectangle.width/100 * 4
                font.bold: true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: rectangle_legenda.verticalCenter
            }
        }
    }
}
