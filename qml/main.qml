import Qt.labs.platform 1.1


SystemTrayIcon {
  visible: true
  id: librasofficeApp
  icon.source: "assets/labis-librasoffice.png"
  tooltip: qsTr("LIBRASOffice - Leitor de telas para língua de sinais")
  property var signalExibition: true

  menu: Menu {
    MenuItem {
      id: item1
      text: qsTr("Parar de exibir sinais")
      onTriggered: {
        if(librasofficeApp.signalExibition == true){
          librasofficeApp.signalExibition = false;
          LIBRASOfficeApp.handleSignalExibition(librasofficeApp.signalExibition)
          item1.text = qsTr("Voltar a exibir sinais");
        }
        else{
          librasofficeApp.signalExibition = true;
          LIBRASOfficeApp.handleSignalExibition(librasofficeApp.signalExibition)
          item1.text = qsTr("Parar de exibir sinais");
        }
      }
    }
    MenuItem {
      text: qsTr("Sobre")
      property string messageText: {
        "LIBRASOffice é um projeto acadêmico de extensão e pesquisa do LabIS/LIpE/UFRJ que tem como objetivo o auxílio de usuários surdos, que não dominam português, mas sim LIBRAS, na utilização do LibreOffice autônomamente.\nPara mais informações, acesse:\n<a href='http://labis.cos.ufrj.br'>http://labis.cos.ufrj.br</a> "
      }
      onTriggered: {
        showMessage("Sobre o LIBRASOffice",messageText,
                    "",
                    8000
                    )
      }
    }

    MenuItem {
      text: qsTr("Fechar")
    //  icon.name: "edit-undo"
      iconSource: "assets/labis-librasoffice.png"
      onTriggered: {
        Qt.quit()
      }
    }
  }
}
